import * as angular from 'angular';
import uiRouter from 'angular-ui-router';
import {ConnectionService} from "./ConnectionService";
import {PlaylistComponent} from "./PlaylistComponent";

export interface Hello {
    name: string,
    message: string
}

export const AppComponent: angular.IComponentOptions = {
    template: `<div>
        <h1>hellofk</h1>
        <div ui-view></div>
    </div>`
};

export const BaardModule = angular.module('BaardMain', [uiRouter])
    .component('playlist', PlaylistComponent)
    .service('connectionService', ConnectionService)
    .config(($stateProvider: angular.ui.IStateProvider,
      $urlRouterProvider: angular.ui.IUrlRouterProvider) => {
        $stateProvider
            .state('playlist', {
                name: 'playlist',
                url: '/playlist',
                component: 'playlist',
            });
        $urlRouterProvider.otherwise('/playlist');
    }).name;