import * as angular from 'angular';
import {Hello} from './app';

export class ConnectionService {
    static $inject: string[] = ['$http'];
    private _helloworld: angular.IHttpPromise<Hello>;

    constructor(private $http: angular.IHttpService) {
        this._helloworld = this.fetchInfo();
    }

    private fetchInfo(): angular.IHttpPromise<Hello> {
        return this.$http.get('http://bard-api.herokuapp.com/info');
            // .then( (response: angular.IHttpPromiseCallbackArg<any>) => {
            //     this._helloworld = response.data;
            //     console.log(response.data);
            // });
    }

    get helloworld(): angular.IHttpPromise<Hello> {
        return this._helloworld;
    }
}