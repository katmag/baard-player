"use strict";
var angular_1 = require('angular');
var angular_ui_router_1 = require('angular-ui-router');
var PlaylistComponent_1 = require("./PlaylistComponent");
var ConnectionService_1 = require("./ConnectionService");
exports.AppComponent = {
    template: "<div>\n        <h1>hellofk</h1>\n        <div ui-view></div>\n    </div>"
};
exports.BaardModule = angular_1.default.module('BaardMain', [angular_ui_router_1.default])
    .component('playlist', PlaylistComponent_1.PlaylistComponent)
    .service('connectionService', ConnectionService_1.ConnectionService)
    .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('playlist', {
        name: 'playlist',
        url: '/playlist',
        component: 'playlist',
    });
    $urlRouterProvider.otherwise('/playlist');
}).name;
