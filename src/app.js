"use strict";
var angular = require('angular');
var angular_ui_router_1 = require('angular-ui-router');
var ConnectionService_1 = require("./ConnectionService");
var PlaylistComponent_1 = require("./PlaylistComponent");
exports.AppComponent = {
    template: "<div>\n        <h1>hellofk</h1>\n        <div ui-view></div>\n    </div>"
};
exports.BaardModule = angular.module('BaardMain', [angular_ui_router_1.default])
    .component('playlist', PlaylistComponent_1.PlaylistComponent)
    .service('connectionService', ConnectionService_1.ConnectionService)
    .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('playlist', {
        name: 'playlist',
        url: '/playlist',
        component: 'playlist',
    });
    $urlRouterProvider.otherwise('/playlist');
}).name;
