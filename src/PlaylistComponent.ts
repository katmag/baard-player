import * as angular from 'angular';
import {ConnectionService} from "./ConnectionService";
import {Hello} from './app';

export class PlaylistController {
    static $inject: string[] = ['connectionService'];
    public hello: Hello;

    constructor(private connectionService: ConnectionService) {
        this.hello = {name: "Loading...", message: "..."};

        this.connectionService.helloworld.then( (response: angular.IHttpPromiseCallbackArg<Hello>) => {
            this.hello = response.data;
            console.log(response.data);
        });
    }
}

export const PlaylistComponent: angular.IComponentOptions = {
    controller: PlaylistController,
    template: `<div>
                WEEfasdfasdfadsfa
                <h1>{{$ctrl.hello.name}}</h1>    
            </div>
        `
};