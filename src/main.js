System.register(['angular', './app'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var angular_1, app_1;
    var appModule;
    return {
        setters:[
            function (angular_1_1) {
                angular_1 = angular_1_1;
            },
            function (app_1_1) {
                app_1 = app_1_1;
            }],
        execute: function() {
            appModule = angular_1.default.module('app', [app_1.BaardModule])
                .component('myApp', app_1.AppComponent);
            angular_1.default.bootstrap(document, [appModule.name]);
        }
    }
});
//# sourceMappingURL=main.js.map