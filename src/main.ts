import angular from 'angular';
import { AppComponent, BaardModule } from './app';

let appModule =
    angular.module('app', [ BaardModule ])
        .component('myApp', AppComponent);

angular.bootstrap(document, [ appModule.name ]);