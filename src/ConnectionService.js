"use strict";
var ConnectionService = (function () {
    function ConnectionService($http) {
        this.$http = $http;
        this._helloworld = this.fetchInfo();
    }
    ConnectionService.prototype.fetchInfo = function () {
        return this.$http.get('http://bard-api.herokuapp.com/info');
        // .then( (response: angular.IHttpPromiseCallbackArg<any>) => {
        //     this._helloworld = response.data;
        //     console.log(response.data);
        // });
    };
    Object.defineProperty(ConnectionService.prototype, "helloworld", {
        get: function () {
            return this._helloworld;
        },
        enumerable: true,
        configurable: true
    });
    ConnectionService.$inject = ['$http'];
    return ConnectionService;
}());
exports.ConnectionService = ConnectionService;
