System.config({
    transpiler: 'typescript',
    typescriptOptions: {
        emitDecoratorMetadata: true
    },
    paths: {
        'npm:': 'https://unpkg.com/'
    },
    map: {
        'app': './src',
        'angular': 'npm:angular/angular.js',
        'angular-ui-router': 'npm:angular-ui-router@1.0.0-beta.3/release/angular-ui-router.js',
        'typescript': 'npm:typescript@2.0.2/lib/typescript.js'
    },
    meta: {
        'angular': {
            format: 'global',
            exports: 'angular'
        }
    },
    //packages defines our app package
    packages: {
        app: {
            main: './main.ts',
            defaultExtension: 'ts'
        }
    }
});